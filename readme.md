# MaxMind GeoIP for Dashboard
Dashboard is a NodeJS project that accompanies a web app you build and provides a complete account system for your users and administration tools.  Dashboard divides your application into two components: a header with account and administrative menus and navigation bar; and a frame for serving content.

The content can come from Dashboard, Dashboard modules, content you added to Dashboard, or an app you built in any other language hosted separately.

This module adds [MaxMind GeoIP](https://maxmind.com) lookup and binds a Country object to each HttpRequest.

#### Dashboard documentation
- [Introduction](https://github.com/userappstore/dashboard/wiki)
- [Configuring Dashboard](https://github.com/userappstore/dashboard/wiki/Configuring-Dashboard)
- [Contributing to Dashboard](https://github.com/userappstore/dashboard/wiki/Contributing-to-Dashboard)
- [Dashboard code structure](https://github.com/userappstore/dashboard/wiki/Dashboard-code-structure)
- [Server request lifecycle](https://github.com/userappstore/dashboard/wiki/Server-Request-Lifecycle)

#### License

This is free and unencumbered software released into the public domain.  The MIT License is provided for countries that have not established a public domain.

## Installation 

You must install [Redis](https://redis.io) and [NodeJS](https://nodejs.org) 8.1.4+ prior to these steps.

    $ mkdir project
    $ cd project
    $ npm init
    $ npm install @userappstore/dashboard
    $ npm install @userappstore/maxmind-geoip
    # create a main.js
    $ node main.js

Your `main.js` should contain:

    const dashboard = require('./index.js')
    dashboard.start(__dirname)

Add this code to require the module in your `package.json`:

    "dashboard": {
      "dashboard-modules": [
        "@userappstore/maxmind-geoip"
      ]
    }

Your sitemap will output the server address, by default you can access it at:

    http://localhost:8000

The first account to register will be the owner and an administrator.

### Example country object

    {
      continent: {
        code: 'NA',
        geoname_id: 6255149,
        names: {
          de: 'Nordamerika',
          en: 'North America',
          es: 'Norteamérica',
          fr: 'Amérique du Nord',
          ja: '北アメリカ',
          'pt-BR': 'América do Norte',
          ru: 'Северная Америка',
          'zh-CN': '北美洲'
        }
      },
      country: {
        geoname_id: 6252001,
        iso_code: 'US',
        names: {
          de: 'USA',
          en: 'United States',
          es: 'Estados Unidos',
          fr: 'États-Unis',
          ja: 'アメリカ合衆国',
          'pt-BR': 'Estados Unidos',
          ru: 'США',
          'zh-CN': '美国'
        }
      },
      registered_country: {
        geoname_id: 6252001,
        iso_code: 'US',
        names: {
          de: 'USA',
          en: 'United States',
          es: 'Estados Unidos',
          fr: 'États-Unis',
          ja: 'アメリカ合衆国',
          'pt-BR': 'Estados Unidos',
          ru: 'США',
          'zh-CN': '美国'
        }
      }
    }
