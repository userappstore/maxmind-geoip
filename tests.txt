
> @userappstore/maxmind-geoip@2018.9.5-1 test /Users/benlowry/CloudStation/workspaces/dashboard/maxmind-geoip
> NODE_ENV=testing mocha src --recursive --timeout 5000



  proxy/x-country-header
    Country#GET
      ✓ should set country data in header

  server/bind-country
    BindCountry#GET
      ✓ should bind country data to req

  /api/maxmind/country
    Country#GET
      ✓ should return country data
      ✓ should bind country data to req


  4 passing (23ms)

