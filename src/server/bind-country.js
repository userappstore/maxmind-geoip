const maxmind = require('../../index.js')

module.exports = {
  before: async (req) => {
    if (!req.ip) {
      return
    }
    req.country = maxmind.CountryLookup.get(req.ip)
  }
}
