const maxmind = require('../../../../../index.js')

module.exports = {
  auth: false,
  get: async (req) => {
    req.country = maxmind.CountryLookup.get(req.ip)
    return req.country
  }
}
