/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/maxmind/country', () => {
  describe('Country#GET', () => {
    it('should return country data', async () => {
      const req = TestHelper.createRequest('/api/user/maxmind/country', 'GET')
      req.ip = '8.8.8.8'
      const country = await req.route.api.get(req)
      assert.strictEqual(country.country.iso_code, 'US')
    })

    it('should bind country data to req', async () => {
      const req = TestHelper.createRequest('/api/user/maxmind/country', 'GET')
      req.ip = '8.8.8.8'
      await req.route.api.get(req)
      assert.strictEqual(req.country.country.iso_code, 'US')
    })
  })
})
