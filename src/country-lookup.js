const maxmind = require('maxmind')
const path = require('path')

module.exports = maxmind.openSync(path.join(__dirname, 'GeoLite2-Country.mmdb'))
